pragma solidity ^0.5.8;

contract SimpleBank {
    struct AccountBalance {
        uint accountBalance;
        bool isEnrolled;
    }
    uint8 private clientCount;
    mapping (address => AccountBalance) private balances;
    address public owner;

    event LogDepositMade(address indexed accountAddress, uint amount);

    constructor() public payable {
        owner = msg.sender;
        clientCount = 0;
    }

    function enroll() public returns (uint) {
        balances[msg.sender].isEnrolled = true;
        clientCount += 1;
        return balances[msg.sender].accountBalance;
    }

    function deposit() public payable returns (uint) {
        require(balances[msg.sender].isEnrolled == true, "Enroll first");
        balances[msg.sender].accountBalance += msg.value;
        emit LogDepositMade(msg.sender, msg.value);
        return balances[msg.sender].accountBalance;
    }

    function withdraw(uint withdrawAmount) public returns (uint remainingBal) {
        require(balances[msg.sender].isEnrolled == true, "Enroll first");
        require(balances[msg.sender].accountBalance > withdrawAmount, "Balance not enough");

        balances[msg.sender].accountBalance -= withdrawAmount;
        msg.sender.transfer(withdrawAmount);

        return balances[msg.sender].accountBalance;
    }

    function balance() public view returns (uint) {
        return balances[msg.sender].accountBalance;
    }

    /// @return The balance of the Simple Bank contract
    function depositsBalance() public view returns (uint) {
        return address(this).balance;
    }
}